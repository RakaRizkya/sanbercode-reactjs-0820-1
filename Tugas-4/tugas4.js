// soal 1
console.log("LOOPING PERTAMA");
var i = 0
while (i < 20) {
  i += 2;
  console.log(String(i) + " - I love coding");
}

console.log("LOOPING KEDUA");
while (i > 0) {
  console.log(String(i) + " - I will become a frontend developer");
  i -= 2;
}

// soal 2
for(var j = 1; j <= 20; j++) {
    if(j % 2 !== 0) {
      if(j % 3 === 0) {
        console.log(String(j) + " - I Love Coding");
      } else {
        console.log(String(j) + " - Santai");
      }
    } else {
      console.log(String(j) + " - Berkualitas");
    }
}

// soal 3
var hashtag = [];
var stairs = "";
for (var i = 1; i <= 7; i++) {
  hashtag.push('#');
  stairs = hashtag.join("")
  console.log(stairs);
} 

// soal 4
var kalimat = "saya sangat senang belajar javascript";
var kata = kalimat.split(" ");
console.log(kata);

// soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
for(var i = 0; i < daftarBuah.length; i++) {
  console.log(daftarBuah[i]);
}