// soal 1
/* release 0 */
class Animal {
    // Code class di sini
    constructor(name) {
        this._name = name
        this.legs = 4
        this.cold_blooded = false
    }
    get name() {
        return this._name
    }

    set name(x) {
        this._name = x
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

/* release 1 */
class Ape extends Animal {
    constructor(cold_blooded) {
        // legs tidak diwariskan karena kera berkaki 2
        super(cold_blooded)
        this.legs = 2  
    }

    yell() {
        return "Auooo"
    }
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell())

class Frog extends Animal {
    constructor(legs) {
        // cold_blooded tidak diwariskan karena kodok berdarah dingin
        super(legs)
        this.cold_blooded = true
    }

    jump() {
        return "hop hop"
    }
}

var kodok = new Frog("buduk")
console.log(kodok.jump())

// soal 2
class Clock {
    constructor({ template }) {
        this.template = template;
    }
  
    render() {
        let date = new Date();
  
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
  
        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
  
        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
      
        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
        console.log(output);
    }
  
    stop() {
        clearInterval(this.timer);
    }
  
    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
  
var clock = new Clock({template: 'h:m:s'});
clock.start(); 