// soal 1
const lingkaran = (radius) => {
    const phi = 22/7;
    let luasLingkaran =  phi * radius ** 2;
    let kelilingLingkaran = 2 * phi * radius;
    return [luasLingkaran, kelilingLingkaran]
}

console.log(`Luas lingkaran adalah sebesar ${lingkaran(14)[0]} cm persegi`);
console.log(`Keliling lingkaran adalah sepanjang ${lingkaran(14)[1]} cm`);

// soal 2
let kalimat = "";
const kata = ["saya", "adalah", "seorang", "frontend", "developer"];

const tambahKata = (kata) => {
    for (let i = 0; i < kata.length; i++) {
        kalimat += `${kata[i]} `;
    }
    return kalimat;
}

tambahKata(kata)
console.log(kalimat)

// soal 3
const newFunction = (firstName, lastName) => {
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: () => console.log(`${firstName} ${lastName}`)
    }
}

newFunction("William", "Imoh").fullName() 

// soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)

// soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)